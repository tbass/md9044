import pandas as pd
import numpy as np
import pickle as pkl
import datetime
import os

from cern_general_devices import scopes
import pyjapc

DEBUG = True

japc = pyjapc.PyJapc(noSet = DEBUG)
japc.setSelector("CPS.USER.MD4")

time_at_script_start = datetime.datetime.now()
RESULTS_DIR = f"results/new_scope/{time_at_script_start.isoformat('T', 'seconds')}"
os.makedirs(RESULTS_DIR, exist_ok=True)

print("SCRIPT START")
print(time_at_script_start.isoformat('T', 'seconds'))
print("="*30)

scope_in = scopes.ScopeOASIS(japc, 'PR.SCOPE71.CH01')
scope_out = scopes.ScopeOASIS(japc, 'PR.SCOPE71.CH02')

scopes = [scope_in, scope_out]

def bxscal_callback(paramName, newValue):
    
    time_at_acq = datetime.datetime.now()

    data = {'deltaCountArray': newValue['deltaCountArray'], 'timeArray': newValue['timeArray']}
    with open(f"{RESULTS_DIR}/{paramName.split('/')[0]}_{time_at_acq.isoformat('T', 'seconds')}.pkl", 'wb') as fpkl:
        pkl.dump(data, fpkl)

    scope_index = int(paramName.split('/')[0][-1])
    scope_data = scopes[scope_index].getParameter()
    with open(f"{RESULTS_DIR}/scope{scope_index}_{time_at_acq.isoformat('T', 'seconds')}.pkl", 'wb') as fpkl:
        pkl.dump(scope_data, fpkl)

    print(f"saved data for {paramName} in \t\t{time_at_acq.isoformat('T', 'seconds')}")


japc.subscribeParam(f"BXSCAL_1100/Acquisition", bxscal_callback)
japc.subscribeParam(f"BXSCAL_1101/Acquisition", bxscal_callback)

japc.startSubscriptions()