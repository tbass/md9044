import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import pandas as pd
import datetime
#import functions as fs
import pickle
from scipy.fft import rfft, rfftfreq
import os

folder_name = "spill_data_bxscal"
os.makedirs(folder_name, exist_ok=True)
root_folder = os.getcwd()
time_title = datetime.datetime.now().isoformat()



japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")

# Data we want to save

fig, ax = plt.subplots(1,1, tight_layout=True, figsize=(7,6))
fig.canvas.manager.set_window_title(f'Spill monitor started at: {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')
fig.suptitle(f'Spill monitor {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')

fig.show()

def myCallback(parameterName, newValue):
    print("!\tcallback")
    ax.clear()

    time = datetime.datetime.now()
    # Append all new data for the new shot
    timestamp_list.append(time)
    for spill_monitor in spill_monitor_name_list:
        try:
            data = japc.getParam(spill_monitor)
        except:
            data = japc.getParam(spill_monitor, timingSelectorOverride="") #EaConfig requires no selector
        spill_monitor_dict[spill_monitor].append(data)

    signal_raw = japc.getParam("BXSCAL_1100/Acquisition#countArray")
    sampling_period_raw = japc.getParam("BXSCAL_1100/EaConfig#counterSampling", timingSelectorOverride="") # microsecond
    sampling_period = round(sampling_period_raw*1e-6, 10) #convert to microsec
    sampling_freq = 1/sampling_period

    string1 = f"Spill monitor {time.strftime('%Y %m %d %Hh%Mm%Ss')}"
    string2 = f"Sampling period = {sampling_period_raw} "+str("$\mu s$")
    string3 = f"Sampling frequency = {sampling_freq*1e-3} " + str("kHz")
    fig.suptitle(string1+"\n"+string2+"\n"+string3)


    signal = np.ones(len(signal_raw))
    for i in range(len(signal_raw)):
        if i > 1:
            signal[i] = signal_raw[i] - signal_raw[i - 1]
        else:
            signal[i] = signal_raw[i]

    d = {'signal': signal}
    df = pd.DataFrame(data=d)

    bound_min = 800
    bound_max = 1800
    fft_period = (bound_max - bound_min)*sampling_period


    nbOfSamples = len(df.signal)
    integrationDuration = nbOfSamples * sampling_period * 1000  # in ms
    t = np.arange(0, integrationDuration, integrationDuration / nbOfSamples)  # time in ms

    signal_np = np.array(df.signal[bound_min:bound_max])
    yf = rfft(signal_np) # You need to convert to a numpy array and not a pandas series
    xf = rfftfreq(len(signal_np), (sampling_period))
    ax.plot(np.array(xf)*1e-3, np.abs(yf), c="m")
    ax.set_xlabel("Frequency [kHz]")
    ax.set_ylabel("Amplitude [arb.]")


    fig.canvas.draw()
    try:
        # Pickle data dump
        pickle.dump((timestamp_list, spill_monitor_dict), open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
    except:
        print('Error on saving data json pickle')
    try:
        # Save figure
        fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
                    transparent=False,
                    bbox_inches='tight');
    except:
        print('error on saving image')

japc.subscribeParam("BXSCINT_1000/Acquisition#countArray", myCallback)
japc.startSubscriptions()
print("!\tsubscription started")
fig.show()
