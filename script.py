import os
import sys
import datetime
import time

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle as pkl
from scipy.fft import rfft, rfftfreq

from cern_general_devices import scopes

import pyjapc

#TODO: DEBUG
global DEBUG
DEBUG = False
USER = 'CPS.USER.MD4'

SPILL_MONITORS = ['BXSCAL_1000', 
                  'BXSCAL_1100']

INT_MONITORS = ['BXSCINT_1000',
                'BXSCINT_1001']

japc = pyjapc.PyJapc(noSet = DEBUG)
if not DEBUG: japc.rbacLogin()
japc.setSelector(USER)

scope_in = scopes.ScopeOASIS(japc, 'PR.SCOPE71.CH01')
scope_out = scopes.ScopeOASIS(japc, 'PR.SCOPE71.CH02')

# PARAMETERS ############################################################
# KNOB              the Plane Strength (gain) that we are changing      #
#                                                                       #
# PARAMETER_MIN     the start value of the parameter sweep              #
# PARAMETER_Max[0]  the end value of the parameter sweep                #
# PARAMETER_STP     the step size of the parameter sweep                #
#                                                                       #
# PARAM_SPACE       the parameter space to sweep over                   #
#   parameter_iterator    an iterator for PARAM_SPACE                   #
#                                                                       #
# CHIRP_START       the start frequency of the chirp                    #
# CHIRP_STOP        the end frequency of the chirp                      #
# CHIRP_INTERVAL    the duration of the chirp (ms)                      #
# ACQ_OFFSET        time to start the chirping (ms)                     #
#                                                                       #
# N_ACQUISITIONS    the number of times to repeat the chirp             #
# N_TURNS           the number of turns to measure for                  #
#########################################################################

KNOB = 'interval' 

# PARAMETER SPACE
PARAMETER_MIN = 1
PARAMETER_MAX = 10
PARAMETER_STP = 1
param_space = np.arange(PARAMETER_MIN, PARAMETER_MAX, PARAMETER_STP)

# ITERATOR
#PARAM_SPACE = np.concatenate((param_space_1, param_space_2))
PARAM_SPACE = param_space
parameter_iterator = iter(PARAM_SPACE)

# CHIRP
CHIRP_START = 0.3   # fractional
CHIRP_STOP = 0.35  # fractional
CHIRP_INTERVAL = PARAM_SPACE[0]  # ms 
ACQ_OFFSET = 1260-235   # ms, start of chirping

# ACQUISITIONS
N_ACQUISITIONS = 500
N_TURNS = 256 # 256 or 512 or 1024 or 2048

try:
    assert CHIRP_INTERVAL * N_ACQUISITIONS < (1790-ACQ_OFFSET)
except AssertionError:
    print(f"! ERROR:\tyou have requested too many acquisitions or too long a chirp interval. \n\t\tEither change the chirp interval to below {(1790-ACQ_OFFSET)/N_ACQUISITIONS} ms \n\t\tor reduce the number of acquisitions to below {int((1790-ACQ_OFFSET)/CHIRP_INTERVAL)}")
    
# DEVICE SETTINGS #######################################################
# QMETER_DEVICE         name of the underlying qmeter2/BBQ device       #
# QMETER_SETTINGS       settings dict for QMETER_DEVICE                 #
#                       'python_name': ('japc_name', value)             #
#########################################################################
    
QMETER_DEVICE = 'PR.BQL72/Setting'
QMETER_SETTINGS = {
    'ex_mode': ('#exMode', 1), # CHIRP
    'acq_on': ('#acqState', 1), # ON
    'acq_mode': ('#acqMode', 0), # Continuous 
    'det_fft_window': ('#fftWindowFunction', 1), # Hann
    'n_measurements': ('#nbOfMeas', N_ACQUISITIONS), ################### NUMBER OF ACQUISITIONS
    'n_turns': ('#nbOfTurns', N_TURNS), ############################################# 256 turns
    'interval': ('#acqPeriod', CHIRP_INTERVAL), ####################### CHIRP DURATION/INTERVAL
    'from': ('#acqOffset', ACQ_OFFSET), ################################ START TIME OF FLAT-TOP
    'ex_h_amplitude': ('#exAmplitudeH', 0.1), ################################ GAIN / AMPLITUDE
    'ex_v_amplitude': ('#exAmplitudeV', 0.06), # ignored
    'ex_h_enable': ('#exEnableH', 'True'), # enable H
    'ex_v_enable': ('#exEnableV', 'False'), # disable V
    'chirp_h_start': ('#chirpStartFreqH', CHIRP_START), ################# CHIRP START FREQUENCY 
    'chirp_h_stop': ('#chirpStopFreqH', CHIRP_STOP), ###################### CHIRP END FREQUENCY
    'chirp_v_start': ('#chirpStartFreqV', 0.25), # ignored
    'chirp_v_stop': ('#chirpStopFreqV', 0.40), # ignored
    'bias_h': ('#biasH', 0), # n/a
    'bias_v': ('#biasV', 0), # n/a
    'time_const_1h': ('#timeConstant1H', 1), # n/a
    'time_const_2h': ('#timeConstant2H', 1), # n/a
    'time_const_1v': ('#timeConstant1V', 1), # n/a
    'time_const_2v': ('#timeConstant2V', 1), # n/a
    'DC_det_1': ('#DCdet1', 0), # n/a
    'DC_det_2': ('#DCdet2', 0), # n/a
    'ex_pattern': ('#exPattern', 1), # concurrent
    'length': ('#exLength', 0), ############################################### Custom Turns
}

global newline
newline = True

# Wrapper for beam presence
def check_beam(func):
    def check_beam_wrapper(*args, **kwargs):
        global newline
        if newline:
            print("\n\n", "="*50)
            newline = False
        elif not newline:
            newline = True
        is_beam_here = np.array(japc.getParam('PR.BCT-ST/Samples#samples'))
        if DEBUG:
            print(f"! DEBUG, BYPASSING CHECK")
            func(*args, **kwargs)
        elif (np.amax(is_beam_here)) > 1:
            print(f"~ CHK:\tmeasured {round(np.amax(is_beam_here), 3)}, callback ok\t:CHECK ~")
            func(*args, **kwargs)
        else:
            print(f"! CHK:\tmeasured {round(np.amax(is_beam_here), 3)}, NO BEAM, SKIPPING\t:CHECK")
    return check_beam_wrapper

# function for updating qmeter device with japc
def set_qmeter(device, settings):
    '''
    Iterates over `settings` and sets the corresponding parameter on `device` with JAPC
    '''
    for setting in settings:
        japc_name, value = settings[setting]
        japc.setParam(device+japc_name, value)

# Set Qmeter to initial value
QMETER_SETTINGS[KNOB] = (QMETER_SETTINGS[KNOB][0], PARAM_SPACE[0])
set_qmeter(QMETER_DEVICE, QMETER_SETTINGS)

# Set up refreshing plots
plt.ion()
fig, ax = plt.subplots(4, 1, figsize=(7, 8), tight_layout=True)
#fig.canvas.set_window_title('F61.XSEC023-I1/IntensityMeasurement#intensity')

# Create empty data list
data = list()

## FFTing
time_at_script_start = datetime.datetime.now()
print(f"\n\n~ INFO:\tscript start\n~ TIME:\t{time_at_script_start.isoformat('T', 'seconds')}")
RESULTS_DIR = f"results/{time_at_script_start.isoformat('T', 'seconds')}"
os.makedirs(RESULTS_DIR, exist_ok=True)
with open(f'{RESULTS_DIR}/initial.pkl', 'wb') as f:
    pkl.dump({
            'time_at_script_start': time_at_script_start,
            'knob': KNOB,
            'param_space': PARAM_SPACE,
            'qmeter_device': QMETER_DEVICE,
            'qmeter_settings': QMETER_SETTINGS, 
            }, f)

timestamp_list = []
spill_monitor_name_lists = [ [f'{d}/Acquisition', f'{d}/EaConfig'] for d in SPILL_MONITORS]
spill_monitor_name_list = [item for sub in spill_monitor_name_lists for item in sub]
spill_monitor_dict = {spill_monitor: [] for spill_monitor in spill_monitor_name_list}

qmeter_data = pd.DataFrame(columns=['variable', 'amplitude', 'timestamp', 'settings'])
with open(f'{RESULTS_DIR}/qmeter.csv', 'w') as f:
    f.write('variable,amplitude,timestamp,settings\n')

# Callback function
@check_beam
def qmeter_callback(name, value):
    '''
    Callback function
    1. collects data from subscribed device and saves to `data`
    2. updates plot
    3. updates qmeter device with next parameter value
    '''
    print("! CALL:\tqmeter callback")
    # Get reading
    # Current knob variable (amplitude, etc)
    if not DEBUG:
        variable = japc.getParam(QMETER_DEVICE+QMETER_SETTINGS[KNOB][0])
    else:
        try:
            variable = parameter
        except UnboundLocalError:
            variable = PARAM_SPACE[0]
    print(f"> SET:\tvariable:\t{variable}")
    print(f"> SET:\tknob\t\t{KNOB}")
    # Recorded intensity
    print(f"> READ:\tintensity:\t{value}")

    
    # Save results
    try:
        new_data = pd.DataFrame([variable, value, datetime.datetime.now().isoformat('T', 'seconds'), QMETER_SETTINGS], index=['variable','value','time','settings']).T
        new_data.to_csv(f'{RESULTS_DIR}/qmeter.csv', mode='a', header=False)
        qmeter_data = pd.concat([qmeter_data, new_data])
    except UnboundLocalError:
        qmeter_data = pd.DataFrame([variable, value, datetime.datetime.now().isoformat('T', 'seconds'), QMETER_SETTINGS], index=['variable','value','time','settings']).T
        qmeter_data.to_csv(f'{RESULTS_DIR}/qmeter.csv', mode='a', header=False)

    for i, scope in enumerate([scope_in, scope_out]):
        get = scope.getParameter()
        scope_data = get[0]
        scope_stamp = get[1]
        with open(f"{RESULTS_DIR}/scope_{i}_{datetime.datetime.now().isoformat('T', 'seconds')}.pkl", 'wb') as fpkl:
            pkl.dump({'data': scope_data, 'time': scope_stamp}, fpkl)
            print(f"~ INFO:\tscope {i} data saved")

        
    # Get next parameter from parameter space
    try:
        parameter = next(parameter_iterator)
    except StopIteration:
        # save data if parameter space finished
        qmeter_data.to_pickle(f"{RESULTS_DIR}/qmeter_{datetime.datetime.now().isoformat('T', 'seconds')}.pkl")
        japc.stopSubscriptions()
        print("! ERROR:\tParameter space exhausted!\t:ERROR")
        print("="*20, "\nEND PROGRAM")
        print("#"*50)
        print("#"*50)
    else:
        # Set qmeter parameter ready for next trigger
        QMETER_SETTINGS[KNOB] = (QMETER_SETTINGS[KNOB][0], parameter)
        QMETER_SETTINGS['n_measurements'] = (QMETER_SETTINGS['n_measurements'][0], int(500/QMETER_SETTINGS['interval'][1]))
        set_qmeter(QMETER_DEVICE, QMETER_SETTINGS)
    #fig.canvas.draw()
    
fft_data = pd.DataFrame(columns=['frequencies', 'amplitudes', 'timestamp', 'settings'])

with open(f'{RESULTS_DIR}/fft.csv', 'w') as f:
    f.write('frequencies,amplitudes,timestamp,settings\n')
    
@check_beam
def bxscal_callback(parameterName, newValue):
    print(f"! CALL:\tbxscal callback")
    ax[0].clear()
    ax[1].clear()
    ax[2].clear()
    time = datetime.datetime.now()
    # Append all new data for the new shot
    timestamp_list.append(time)
    for spill_monitor in spill_monitor_name_list:
        try:
            data = japc.getParam(spill_monitor)
        except:
            data = japc.getParam(spill_monitor, timingSelectorOverride="") #EaConfig requires no selector
        spill_monitor_dict[spill_monitor].append(data)

    for index, spill_monitor in enumerate(SPILL_MONITORS):
        print(f"! CALL:\tmonitoring {spill_monitor}")
        signal_raw = japc.getParam(f"{spill_monitor}/Acquisition#countArray")
        time_raw = japc.getParam(f"{spill_monitor}/Acquisition#timeArray")
        sampling_period_raw = japc.getParam(f"{spill_monitor}/EaConfig#counterSampling", timingSelectorOverride="") # microsecond
        sampling_period = round(sampling_period_raw*1e-6, 10) #convert to microsec
        sampling_freq = 1/sampling_period

        ax[index].set_title(f"{spill_monitor} @ {sampling_period_raw}$\mu s$")

        signal = np.ones(len(signal_raw))
        for i in range(len(signal_raw)):
            if i > 1:
                signal[i] = signal_raw[i] - signal_raw[i - 1]
            else:
                signal[i] = signal_raw[i]

        d = {'signal': signal}
        df = pd.DataFrame(data=d)

        #ax[index].plot(df.signal, color="b")
        bound_min = 1400
        bound_max = 1400+5000
        
        fft_period = (bound_max - bound_min)*sampling_period
        ax[index].plot(np.array(time_raw)[bound_min:bound_max], df.signal[bound_min:bound_max], c="m", label=f"FFT bounds: {bound_min}-{bound_max} {fft_period*1e3} ms")
        
        ax[index].plot(np.array(time_raw), np.array(signal_raw))
        ax[index].set_xlabel("Samples [ ]")
        ax[index].set_ylabel("Amplitude [arb.]")
        ax[index].legend()

        nbOfSamples = len(df.signal)
        integrationDuration = nbOfSamples * sampling_period * 1000  # in ms
        t = np.arange(0, integrationDuration, integrationDuration / nbOfSamples)  # time in ms

        time_np = np.array(t[bound_min:bound_max])
        signal_np = np.array(df.signal[bound_min:bound_max])
        yf = rfft(signal_np) # You need to convert to a numpy array and not a pandas series
        xf = rfftfreq(len(signal_np), (sampling_period))
        #ax[2].plot(np.array(xf)*1e-3, np.abs(yf), c="m")
        #ax[2].set_xlabel("Frequency [kHz]")
        #ax[2].set_ylabel("Amplitude [arb.]")
        #ax[2].set_xlim(0, 1.2)

        signal_df = {'countArray': signal_np, 'timeArray': np.array(time_raw)[bound_min:bound_max], 'rawCounts': np.array(signal_raw), 'rawTime': np.array(time_raw)}
        with open(f"{RESULTS_DIR}/{spill_monitor}_signal_{datetime.datetime.now().isoformat('T', 'seconds')}.pkl", 'wb') as fpkl:
            pkl.dump(signal_df, fpkl)
        try:
            new_fft_data = pd.DataFrame([np.array(xf)*1e-3, np.abs(yf), datetime.datetime.now().isoformat('T', 'seconds'), {'sampling period': sampling_period, 'sampling bounds': (bound_min, bound_max)}], index=['variable','value','time','settings']).T
            new_fft_data.to_csv(f"{RESULTS_DIR}/{spill_monitor}_fft.csv", mode='a', header=False)
            fft_data = pd.concat([fft_data, new_fft_data])
            new_fft_data.to_pickle(f"{RESULTS_DIR}/{spill_monitor}_fft_{datetime.datetime.now().isoformat('T', 'seconds')}.pkl")
        except UnboundLocalError:
            fft_data = pd.DataFrame([np.array(xf)*1e-3, np.abs(yf), datetime.datetime.now().isoformat('T', 'seconds'), {'sampling period': sampling_period, 'sampling bounds': (bound_min, bound_max)}], index=['variable','value','time','settings']).T
            fft_data.to_csv(f"{RESULTS_DIR}/{spill_monitor}_fft.csv", mode='a', header=False)
            fft_data.to_pickle(f"{RESULTS_DIR}/{spill_monitor}_fft_{datetime.datetime.now().isoformat('T', 'seconds')}.pkl")
            
    blm = japc.getParam('PR.BCT-ST/Samples')
    ax[2].plot(blm['samples'])
    ax[2].set_xlabel("time")
    ax[2].set_ylabel("I")
    ax[3].plot(blm['samples'], linewidth=0.5)
    with open(f"{RESULTS_DIR}/blm_{datetime.datetime.now().isoformat('T', 'seconds')}.pkl", 'wb') as fpkl:
        pkl.dump(blm, fpkl)
        print(f"~ INFO:\tblm data data saved")
    
    fig.canvas.draw()
    fig.savefig(f"{RESULTS_DIR}/{parameterName.split('/')[0]}_{datetime.datetime.now().isoformat('T', 'seconds')}.png")

@check_beam
def bxscint_callback(parameterName, newValue):
    print(f"! CALL:\tbxscint callback")
    for index, bxscint in enumerate(INT_MONITORS):
        print(f"! CALL:\tmonitoring {bxscint}")
        signal_raw = japc.getParam(f"{bxscint}/Acquisition#countArray")
        settings_raw = japc.getParam(f"{bxscint}/EaConfig", timingSelectorOverride="")
        full_signal_raw = japc.getParam(f"{bxscint}/Acquisition")
        signal_np = np.array(signal_raw)
        signal_df = {'countArray': signal_np, 'EaConfig': settings_raw, 'Acquisition': full_signal_raw}
        with open(f"{RESULTS_DIR}/{bxscint}_signal_{datetime.datetime.now().isoformat('T', 'seconds')}.pkl", 'wb') as fpkl:
            pkl.dump(signal_df, fpkl)


    
japc.subscribeParam("F61.XSEC023-I1/IntensityMeasurement#intensity", qmeter_callback)
japc.subscribeParam(f"BXSCAL_1100/Acquisition", bxscal_callback)
japc.subscribeParam(f"BXSCINT_1000/Acquisition", bxscint_callback)

print("\n")
japc.startSubscriptions()
    
