import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import pandas as pd
import datetime
import pickle
from scipy.fft import rfft, rfftfreq

time_at_script_start = datetime.datetime.now()

DEBUG = True
USER = 'CPS.USER.EAST3'
japc = pyjapc.PyJapc(noSet = DEBUG)
if not DEBUG: japc.rbacLogin()
japc.setSelector(USER)

# First data for the heatmap
signal_1000 = japc.getParam("BXSCINT_1000/Acquisition#countArray")
fifoFreq = japc.getParam("BXSCINT_1000/Acquisition#fifoFreq")

nbOfSamples = len(signal_1000)
integrationDuration = nbOfSamples*(1/fifoFreq)*1000 # in ms

heatmap = np.reshape(signal_1000, (1, nbOfSamples))
time_list = [time_at_script_start.strftime("%Hh%Mm%Ss")]

# Data we want to save
timestamp_list = []
spill_monitor_name_list = ["BXSCINT_1000/Acquisition",
                       "BXSCINT_1001/Acquisition",]
spill_monitor_dict = {spill_monitor: [] for spill_monitor in spill_monitor_name_list}

fig, ax = plt.subplots()

def myCallback(parameterName, newValue):

    global heatmap
    global time_list

    ax.clear()
    time = datetime.datetime.now()

    time_list.append(time.strftime("%Hh%Mm%Ss"))
    fig.suptitle(time.strftime("Spill waterfall East Area \n Most recent acquisition: %Hh%Mm%Ss"))

    t = np.arange(0, integrationDuration, integrationDuration / nbOfSamples)# time in ms

    # Raw data
    signal_1000 = japc.getParam("BXSCINT_1000/Acquisition#countArray")
    signal_1001 = japc.getParam("BXSCINT_1001/Acquisition#countArray")
    fifoFreq = japc.getParam("BXSCINT_1000/Acquisition#fifoFreq")


    # Append all new data for the new shot
    timestamp_list.append(time)
    for spill_monitor in spill_monitor_name_list:
        data = japc.getParam(spill_monitor)
        spill_monitor_dict[spill_monitor].append(data)

    d = {'t': t, 'signal_1000': signal_1000, 'signal_1001': signal_1001}
    df = pd.DataFrame(data=d)
    # Rolling average

    bound_min = 300
    bound_max = 800

    
    yf = rfft(df.signal_1000[bound_min:bound_max])
    xf = rfftfreq(len(df.signal_1000[bound_min:bound_max]), (1/fifoFreq))
    ax.plot(np.array(xf)*1e-3, np.abs(yf), c="m")
    ax.set_xlabel("freq [kHz]")
    ax.set_ylabel("Amplitude [arb.]")
    ax.set_ylim(0, 20000)

    fig.canvas.draw() # Update the plot

    # try:
    #     # Pickle data dump
    #     pickle.dump((timestamp_list, spill_monitor_dict), open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
    # except:
    #     print('Error on saving data json pickle')
    # try:
    #     # Save figure
    #     fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
    #                 transparent=False,
    #                 bbox_inches='tight');
    # except:
    #     print('error on saving image')

japc.subscribeParam("BXSCINT_1000/Acquisition#countArray", myCallback)
japc.startSubscriptions()
fig.show()
