# Wednesday

## CHANGING INTERVAL

0.32 to 0.34

- `2022-11-16T14:05:01.560634`
    - Interval scan from 1ms to 10ms at 0.1 gain

- `2022-11-16T14:21:14.628240`
    - Interval scan from 1ms to 50ms at 0.1 gain
    - XXXXXXXX
    
- `2022-11-16T14:45:27.566973`
    - Interval scan from 1ms to 50ms in steps of 4 at 0.1 gain
    - Ignore 37ms
    
- `2022-11-16T15:06:42.559961`
    - Interval scan from 1ms to 50ms in steps of 5 at 0.1 gain
    - But with better FFT bounds
    
- `2022-11-16T15:52:43.687625`
    - Interval scan from 1ms to 50ms in steps of 5 at 0.07 gain

## CHANGING CHIRP

- `2022-11-16T16:08:55.274178`
    - Change chirp range from below
    - 0.2 to 0.333333 in steps of 0.01 from below to 0.35 at 0.1 gain
    
- `2022-11-16T16:54:26.177566`
    - Same but at 0.2 gain
    
*from above*

- `2022-11-16T17:22:54.903784`
    - Change chirp range from above
    - 0.2 to 0.34 -thru-> 0.44 in steps of 0.1 from bottom at 0.1 gain
    - OASIS at 250V/div, 10 divs total on dispaly
    
### Chirp Test
    
- `2022-11-16T17:33:19.566117`
    - Chirp from 0.1 to 1, 250V/div

- `2022-11-16T17:34:33.302434`
    - Same, 62.5V/div
    - https://logbook.cern.ch/elogbook-server#/fullScreenImg?img=2690951
    
## CHANGING GAIN

- `2022-11-16T17:10:23.770610`
    - Gain scan from 0.1 to 1 in steps of 0.1
    - Interval 1ms, chirp 0.2 to 0.4
    - OASIS: 250V/div, 10 divs
    - So a gain of 1 is pm 1200V, 2400V pp

# Thursday

## Changing Interval

- Interval scan from 1ms to 10ms in steps of 1ms
    - `2022-11-17T12:12:54.043774`
    - 0.1 Gain
    - .3-.35 chirp
    - 500 acq
    - 256 turns
    - Scope looking at one chirp, 250V/div, 80us/div
    - Scope at 1260
    
- Repeat of above but with scope looking at the whole thing
    - `2022-11-17T12:27:29.119159`
    - 0.1 Gain
    - etc
    - Scope looking at whole chirp, 62.5V/div, 50ms/div
    - Scope at 1260
    
- Interval scan from 10ms to 50ms in steps of 5ms
    - `2022-11-17T12:43:05.943656`
    - Same as above
    - Scope looking at one chirp, 62.5V/div, 80us/div
    - Scope at 1260
    
- Repat of above but with scope looking at the whole thing
    - `2022-11-17T13:27:16.876335`
    - 0.1 Gain
    - etc
    - Scope looking at whole chirp, 62.5V/div, **80ms/div**
    - **SCOPE AT 1250**
    
## Changing Turns

- Turns scan from 256 to 16384
    - `2022-11-17T14:18:23`
    - 10 acquisitions
    - 50ms interval
    - Chirp from .3 to .35
    - Scope looking at whole sequence, 50ms/div, 62.5V/div

- Another one at 0.2
    - `2022-11-17T14:29:38`
    
- Another one at 0.1
    - `2022-11-17T14:39:231`
    - But with 5 acquisitions at 100ms interval
    - Going up to 32768
    
- Another one at 0.2
    - `2022-11-17T14:47:03`
    - Otherwise, same as above
    
## Changing Gain

- Gain scan from 0.1 to 1
    - `2022-11-17T14:58:53`
    - 500 Acqs at 1ms
    - Chirp .3 to .35
    - OASIS showing one single chirp, 80us/div, 625V/div
    
## Changing Chirp

- Chirp scan changing below
    - `2022-11-17T15:09:33`
    - Gain 0.1
    - 500 acqs at 1ms
    - 0.2 -> 0.3 to 0.35
    - Scope on one chirp

- Same at 0.2 gain
    - `2022-11-17T15:20:32`
    
- Chirp scan changing above
    - `2022-11-17T15:30:03`
    - 0.1 gain
    - 0.3 to 0.35 -> 0.45
    - Otherwise same
    
- Same at 0.2 gain
    - `2022-11-17T15:38:51`
    - Figures now include historical

- Same at 0.3 gain
    - `2022-11-17T15:49:58`

- Same with OASIS scope set to 125V/div
    - `2022-11-17T16:07:11`
    - And better plots
    
# Tuesday

## Changing interval

- Interval from 1 to 15ms in 1ms steps
    - `2022-11-22T10:32:16`
    - Gain 0.07
    - Chirp range .3 to .35
    - Scope on Chirp

- Repeat at 0.07 again
    - `2022-11-22T10:43:54`
    - Scope on Chirp
    
- Repeat at 0.05
    - `2022-11-22T10:56:42`
    - Scope on Chirp
    
- Repeat at 0.08
    - `2022-11-22T11:11:12`
    - Scope on Chirp
    
- Repeat at 0.08 again WITH OASIS CONNECTED
    - `2022-11-22T11:28:50`
    - 80us/div horizontal, 62.5V/div vertical
    - 1260 offset
    - Looking at a single chirp
    - CH1/scope0: In
    - CH2/scope1: Out
    - Scope on Chirp
    
**updated plot method here**

- Repeat at 0.09 gain 
    - `2022-11-22T11:46:50`
    - Partial Data
    - Scope on Chirp
    
*scans from 1 to 10ms*

- 0.050 gain: `2022-11-23T14:34:55` maybe not   : SCOPE ON EX `used in analysis`
- 0.075 gain: `2022-11-23T15:08:29`             : SCOPE ON EX `used in analysis`
- 0.100 gain: `2022-11-23T14:44:26` maybe not   : SCOPE ON EX `used in analysis`
- 0.150 gain: `2022-11-23T15:16:55` unfinished  : SCOPE ON EX `used in analysis`

## Changing Gain

**from 0.05 to 0.15**

- 6ms at 2048 turns 
    - Scope looking at whole chirp
    - `2022-11-23T10:36:24`
    - .3 to .35
- 3ms at 1024 turns
    - Scope looking at whole chirp
    - `2022-11-23T10:53:27`
    - .3 to .35
- 1ms at 397 turns
    - Scope looking at whole chirp
    - .3 to .35
    - `2022-11-23T11:04:28`
    

