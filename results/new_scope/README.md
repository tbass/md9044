# Results

## 22 Nov

### Changing Freq Dev

- `2022-11-22T16:33:05` 
    - 1V gain
    - at 1kHz chirp rate around 129.3 kHz
    - Change Freq Dev from 2 to 20 in steps of 2

### Changing interval (CH02)

- `2022-11-22T16:51:30`
    - 1V gain
    - 0.5, 1, 2, 3, 4, 5 kHz
    - chirp frequencies around 129.2 with 2 khz freq dev
    
- `2022-11-22T17:07:53`
    - Repeat at .5V
    - 0.5, 1, 2, 3, 4, 5 kHz
    
- `2022-11-22T17:26:17`
    - IGNORE FIRST
    - Back at 1Vpp
    - 1kHz Freq Dev around 129.3kHz

## 23 Nov

### Changing Interval (CH02)

**Ignore First Result**

- `2022-11-23T11:44:01`
    - 4kHz deviation around 132.566 kHz
    - 1Vpp gain
    - 8ms, 4ms, 2ms, 1ms, 0.5ms, 0.25ms
    - 0.125kHz, 0.25kHz, 0.5kHz, 1kHz, 2kHz, 4kHz
    
- `2022-11-23T13:59:33`
    - Repeat repeat (repeat) but also with 8 kHz
    
### Changing Freq Dev

**Ignore First Result**

- `2022-11-23T13:18:15`
    - around 132.566 kHz
    - 1Vpp gain
    - 4 kHz interval
    - 8, 4, 2, 1, 0.5 kHz Freq Dev
    
- `2022-11-23T13:53:59`
    - Repeat repeat (repeat)
    
- `2022-11-23T13:35:18`
    - Same at 2kHz interval

- `2022-11-23T14:07:09`
    - Repeat repeat (repeat)

- `2022-11-23T13:40:13`
    - Same at 1kHz interval
    
- `2022-11-23T13:48:19`
    - Repeat repeat (repeat)
    
## Changing Gain

- `2022-11-23T14:13:40`
    - 1kHz interval
    - 132.566 kHz frequency with a freq dev of 4kHz
    - Gain at 1., .8, .6, .4, .2, 0.