# Intensity Response to Changing Gain

**Hypothesis: extracted beams are lower intensity with less time at resonance.**

1. Change chirp from below:
    
    With a beam at 0.5 gain, 512 turns, and 1ms chirp, and chirp end at 0.35
    
    Change chirp start from 0.20 to 0.33333

    ```
    KNOB = 'chirp_h_start'

    PARAMETER_MIN = 0.20
    PARAMETER_MAX = 1/3
    PARAMETER_STP = 0.01

    CHIRP_START = PARAM_SPACE[0]   # fractional
    CHIRP_STOP = 0.35  # fractional
    CHIRP_INTERVAL = 1  # ms 
    ACQ_OFFSET = 1260-235   # ms, start of chirping
    ```
    
2. Change chirp from above
    
    With a beam at 0.5 gain, 512 turns, and 1ms chirp, and chirp start at 0.32
    
    Change chirp end from 0.3333333 to 0.40

    ```
    KNOB = 'chirp_h_stop'

    PARAMETER_MIN = 0.34
    PARAMETER_MAX = 0.44
    PARAMETER_STP = 0.01

    CHIRP_START = 0.32   # fractional
    CHIRP_STOP = PARAM_SPACE[0]  # fractional
    CHIRP_INTERVAL = 1  # ms 
    ACQ_OFFSET = 1260-235   # ms, start of chirping
    ```
    
3. Change chirp around centre
    
    Centre the chirp frequency range on 0.3333333
    
    Change the width of the chirp range from 0.01 to 0.2