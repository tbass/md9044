import numpy as np
import scipy as sp

def generate_chirp(
        chirp_turns=256,
        chirp_rep_time=0.001,
        turn_freq=470151.0308,
        chirp_start_freq=2e4,
        chirp_stop_freq=8e4,
        sampling_freq=1e6):
    chirp_length_time = chirp_turns / turn_freq
    # Generate one chirp
    chirp_time_samples = np.arange(0, chirp_length_time, 1/sampling_freq)
    chirp_signal_samples = sp.signal.chirp(chirp_time_samples, chirp_start_freq, chirp_length_time, chirp_stop_freq)

    # Generate one repetition
    rep_time_samples = np.arange(0, chirp_rep_time, 1/sampling_freq)[:-1] # cut off the end for the next repetition
    rep_signal_samples = np.pad(chirp_signal_samples, 
                                (0, int(chirp_rep_time * sampling_freq - chirp_length_time * sampling_freq)), 
                                'constant', 
                                constant_values=(0, 0))
    
    return rep_time_samples, rep_signal_samples

def generate_chirp_signal(
        chirp_turns=256,
        chirp_repetitions=500,
        chirp_rep_time=0.001,
        turn_freq=470151.0308,
        chirp_start_freq=2e4,
        chirp_stop_freq=8e4,
        sampling_freq=1e6):
    """
    Generate a chirp signal for the simulation.
    ---
    Returns:
    time_samples: np.ndarray [s]
        Time samples of the signal
    signal_samples: np.ndarray [1]
        Signal samples
    total_turns: float [1]
        Total number of turns in the signal
    ---
    Parameters:
    chirp_turns: int [1]
        Number of turns that one chirp lasts for. Default: 256
    chirp_repetitions: [1]
        Number of chirp repetitions. Default: 500
    chirp_rep_time: float [s]
        Time between chirp repetitions. Default: 0.001 (1ms)
    turn_freq: float [Hz]
        Frequency of one turn in the machine. Default: 470151.0308
    chirp_start_freq: float [Hz]
        Start frequency of the chirp. Default: 2e4
    chirp_stop_freq: float [Hz]
        Stop frequency of the chirp. Default: 8e4
    sampling_freq: float [Hz]
        Sampling frequency of the signal. Default: 1e6
    """
    total_signal_time = chirp_repetitions * chirp_rep_time
    chirp_length_time = chirp_turns / turn_freq
    # Generate one chirp
    chirp_time_samples = np.arange(0, chirp_length_time, 1/sampling_freq)
    chirp_signal_samples = sp.signal.chirp(chirp_time_samples, chirp_start_freq, chirp_length_time, chirp_stop_freq)

    # Generate one repetition
    rep_time_samples = np.arange(0, chirp_rep_time, 1/sampling_freq)[:-1] # cut off the end for the next repetition
    rep_signal_samples = np.pad(chirp_signal_samples, 
                                (0, int(chirp_rep_time * sampling_freq - chirp_length_time * sampling_freq)), 
                                'constant', 
                                constant_values=(0, 0))
    
    # Generate full signal
    time_samples = np.arange(0, total_signal_time, 1/sampling_freq)[:-1] # cut off the end for the next repetition
    signal_samples = np.pad(
        rep_signal_samples,
        (0, len(rep_signal_samples)*(chirp_repetitions-1)),
        'wrap'
    )

    # calculate the number of turns
    total_time = chirp_rep_time * chirp_repetitions
    total_turns = total_time * turn_freq
    return time_samples, signal_samples, total_turns

