import pyjapc
import pandas as pd
import datetime
import numpy as np

japc = pyjapc.PyJapc(noSet = True)
japc.setSelector('CPS.USER.MD3')

def check(a, b):
    is_beam_here = np.array(japc.getParam('PR.BCT-ST/Samples#samples'))
    print((np.amax(is_beam_here)))
    
    
japc.subscribeParam('F61.XSEC023-I1/IntensityMeasurement#intensity', check)
japc.startSubscriptions()
